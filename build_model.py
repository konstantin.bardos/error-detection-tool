import os
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import json
import datetime
import pickle

from matplotlib.backends.backend_pdf import PdfPages

from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets
from build_shape import ShapelBuilder
from build_shape import Shape

from evaluate_kinetic_parameters import KineticFeatureBuilder

from persistence_manager import obj_load, obj_save, obj_load_multiple

from sklearn.ensemble import RandomForestClassifier
from sklearn.calibration import CalibratedClassifierCV
from sklearn.metrics import accuracy_score

# Settings
matplotlib.style.use('ggplot')
pd.set_option('display.expand_frame_repr', False)

class FigureBuilder:
    @staticmethod
    def create_figure(id, name=None):
        # Create new figure
        plt.figure(id, figsize=(19, 10))
        plt.ylim(0, 1050)
        plt.ylim(0, 800)

        if name is not None:
            fig = plt.gcf()
            fig.canvas.set_window_title(name)

    @staticmethod
    def create_3Dfigure(id, name=None, elev=25, azim=-135):
        # Create new 3D figure
        fig = plt.figure(id, figsize=(14, 10))
        plt.clf()
        axes = Axes3D(fig, rect=[0, 0, 1, 1], elev=elev, azim=azim)
        plt.cla()

        axes.set_xlabel('x')
        axes.set_ylabel('y')
        axes.set_zlabel('t')
        if name is not None:
            fig = plt.gcf()
            fig.canvas.set_window_title(name)
        return axes

    @staticmethod
    def plot_single(id, shape, name=None):
        FigureBuilder.create_figure(id, name)
        # Plot shape
        x = shape.data[:, 0]
        y = shape.data[:, 1]
        plt.plot(x, y)

    @staticmethod
    def plot_multiple(id, shapes, name=None):
        FigureBuilder.create_figure(id, name)
        for shape in shapes:
            # Plot shape
            plt.plot(shape.x, shape.y)

    @staticmethod
    def save(name="figure"):
        curr_time = datetime.datetime.today()
        curr_time_str = "%02d-%02d-%02d[%02d-00]" % (curr_time.year, curr_time.month, curr_time.day, curr_time.hour)

        pp = PdfPages('multipage.pdf')
        plt.savefig(pp, format='pdf')

        plt.savefig("./Figures/" + curr_time_str + "-" + name + ".pdf")

    @staticmethod
    def show():
        plt.show()

class Model:

    def __init__(self, model='sine'):

        self.name = 'ErrorDetectionTool v0.99b'

        if model == 'fork':
            path_shape_original = "./ShapeRecordedCSV/"
            path_shapes_validation = "./ShapeRecordedCSV/forks_validation/"
            path_shapes_correct = "./ShapeRecordedCSV/forks_correct/"
            path_shapes_error = "./ShapeRecordedCSV/forks_error/"

            self.shape_orig = ShapelBuilder.get_single_original_csv_shape(path_shape_original, "fork_original.csv")
            self.shapes_correct = ShapelBuilder.get_multiple_csv_shapes(path_shapes_correct)
            self.shapes_error = ShapelBuilder.get_multiple_csv_shapes(path_shapes_error)
            self.shapes_validation = ShapelBuilder.get_multiple_csv_shapes(path_shapes_validation)

        self.shape_orig = obj_load('./ShapeObj/sine_trace.pkl')
        self.shapes_correct = obj_load_multiple('./ShapeObj/sine_correct/', '.pkl')
        self.shapes_error = obj_load_multiple('./ShapeObj/sine_error/', '.pkl')
        self.shapes_validation = obj_load_multiple('./ShapeObj/sine_validation/', '.pkl')

        self.plotter = FigureBuilder()
        self.fignum = 1

        self.go()

    def train_kmeans(self, plot=False):
        X = self.shape_orig.data

        mdl_kmeans = KMeans(n_clusters=70)
        mdl_kmeans.fit(X)

        l = mdl_kmeans.labels_
        c = mdl_kmeans.cluster_centers_

        # print(l)

        if plot:
            # Plotting original K-means clusters
            self.fignum += 1
            ax = self.plotter.create_3Dfigure(self.fignum, "Original K-means")
            ax.scatter(X[:, 0], X[:, 1], X[:, 1], c=l.astype(np.float))

            # Plotting original centroids
            ax.scatter(c[:, 0], c[:, 1], c[:, 1], s=300, facecolors='none', edgecolors='b')
            self.plotter.save("orig_means")

        # Save model, centroids and labels
        self.mdl_kmeans = mdl_kmeans
        self.shape_orig.labels = l
        self.shape_orig.centroids = c

    def train_knn(self):
        mdl = KNeighborsClassifier(n_neighbors=3)
        mdl.fit(self.shape_orig.data, self.shape_orig.labels)
        self.mdl_knn = mdl

    def concatenate_arrays(self, arr1, arr2):
        arr2 = np.array([arr2]).T
        return np.concatenate((arr1, arr2), axis=1)

    def classify_shapes(self, shapes, name, plot=False):

        if plot:
            self.fignum += 1
            self.plotter.create_figure(self.fignum, "classified_knn_" + name)

        for shape in shapes:

            s = shape.data
            x_ = s[:, 0:2]
            y_ = self.mdl_knn.predict(x_)

            x = s[:, 0]
            y = s[:, 1]
            l = y_
            c = self.shape_orig.centroids

            # rows = len(s)

            if plot:
                plt.scatter(x, y, c=y_)

            shape.labels = l
            shape.data = self.concatenate_arrays(shape.data, l)

        if plot:
            plt.scatter(c[:, 0], c[:, 1], s=400, facecolors='none', edgecolors='b')
            self.plotter.save("classified_knn_" + name)

    def build_cluster_array(self, shapes, cluster_array):
        for shape in shapes:
            features = KineticFeatureBuilder.get_features_from_shape(shape, self.shape_orig)
            cluster_array = (np.vstack((cluster_array, features)))
        return cluster_array

    def train_rf(self, plot=False):

        ### Training data
        ### Build feature cluster array from correct and error shapes

        array_train = np.empty((0, 16), dtype=float)
        array_train = self.build_cluster_array(self.shapes_correct, array_train)
        array_train = self.build_cluster_array(self.shapes_error, array_train)

        ### Validation data
        array_test = np.empty((0, 16), dtype=float)
        array_test = self.build_cluster_array(self.shapes_validation, array_test)

        X_train, y_train = array_train[:, 0:15], array_train[:, 15]
        X_test, y_test = array_test[:, 0:15], array_test[:, 15]

        ### Build RF model
        clf = RandomForestClassifier(n_estimators=100)
        clf.fit(X_train, y_train)
        sig_clf = CalibratedClassifierCV(clf, method="sigmoid", cv="prefit")
        sig_clf.fit(X_train, y_train)

        y_pred = sig_clf.predict(X_test)
        # score = accuracy_score(y_test, y_pred)
        score = clf.score(X_test, y_test)

        self.score = score

        ### Save RF model

        self.mdl_rf = sig_clf

        # print(y_test)
        # print(y_pred)
        # print(score)

        if True:
            for shape in self.shapes_validation:

                array_test = np.empty((0, 16), dtype=float)
                array_test = self.build_cluster_array([shape], array_test)
                X_test, y_test = array_test[:, 0:15], array_test[:, 15]

                y_pred = sig_clf.predict(X_test)
                y_prob = sig_clf.predict_proba(X_test)

                # centroids_mask = np.where(y_pred == 1)
                # centroids = self.shape_orig.centroids
                # err_centroids = centroids[centroids_mask]

                centroids_mask = y_prob[:, 1] > 0.98
                centroids = self.shape_orig.centroids
                err_centroids = centroids[centroids_mask]
                # self.err_centroids = err_centroids

                # print(centroids_mask)
                # print(err_centroids)
                # print(type(centroids))
                # print(centroids)
                print(y_prob)

                if plot:
                    name = shape.filename
                    self.fignum += 1
                    figname = "mdl_predicted_errors_" + str(self.fignum)
                    self.plotter.create_figure(self.fignum, figname)

                    s = shape.data
                    x = s[:, 0]
                    y = s[:, 1]

                    ### Plot shape
                    plt.scatter(x, y, c='k')

                    ### Plot clusters with errors

                    c = err_centroids

                    plt.scatter(c[:, 0], c[:, 1], s=400, facecolors='none', edgecolors='b')

                    ### Save figure
                    self.plotter.save(figname)

        ### Show array of clusters

        if False:
            names = [
                "res_trajlength",
                "res_timeinterval",
                "res_velocity_mass",
                "res_acc_mass",
                "res_press_dif",
                "res_angle_mass",
                "res_jerkmass",
                "res_avg_velo",
                "res_avg_acc",
                "res_avg_slope",
                "res_avg_slope_dif",
                "res_avg_press_dif",
                "res_avg_jerk",
                "centr_x",
                "centr_y",
                "err"]

            cluster_array_pd = pd.DataFrame(data=array_test, columns=names)
            # print(cluster_array_pd)

    def go(self):

        self.train_kmeans(plot=False)
        self.train_knn()

        self.classify_shapes(self.shapes_correct, "correct", plot=False)
        self.classify_shapes(self.shapes_error, "error", plot=False)
        self.classify_shapes(self.shapes_validation, "validation", plot=False)
        self.train_rf(plot=False)

        print("Model score: %f" % self.score)
        self.plotter.show()

        ### Classification JSON -> result

    def predict(self, json_input):

        # Import JSON data
        if True:
            drawing = json_input['data']
            points = []

            for stroke in drawing:
                for point in stroke:

                    x = point['x']
                    y = point['y']
                    t = point['time']
                    p = point['force']

                    current_point = [x, y, t, p]
                    points.append(current_point)

        # Cluster imported shape into segments
        if True:
            self.shape_to_predict = Shape(np.asarray(points), 'to_predict')
            self.classify_shapes([self.shape_to_predict], "predict", plot=False)
            array_test = np.empty((0, 16), dtype=float)

        # Calculate kinematic features
        array_test = self.build_cluster_array([self.shape_to_predict], array_test)

        # Predict errors
        if True:
            X_test = array_test[:, 0:15]
            y_prob = self.mdl_rf.predict_proba(X_test)
            centroids_mask = y_prob[:, 1] > 0.98
            centroids = self.shape_orig.centroids
            err_centroids = centroids[centroids_mask]

            print("Predicted centroids:")
            print(err_centroids)

        # Print to PDF
        if False:
            shape = self.shape_to_predict
            name = shape.filename
            self.fignum += 1
            figname = "mdl_predicted_errors_" + str(self.fignum)
            self.plotter.create_figure(self.fignum, figname)

            s = shape.data
            x = s[:, 0]
            y = s[:, 1]

            ### Plot shape
            plt.scatter(x, y, c='k')

            ### Plot clusters with errors

            c = err_centroids

            plt.scatter(c[:, 0], c[:, 1], s=400, facecolors='none', edgecolors='b')

            ### Save figure
            self.plotter.save(figname)

        # Return prediction in JSON
        arr = []
        for elem in err_centroids:

            circle = {}
            circle['center'] = {}
            circle['center']['x'] = elem[0]
            circle['center']['y'] = elem[1]
            circle['radius'] = 40
            arr.append(circle)

        json_out = json.dumps({'data' : arr})
        return json_out


# Create and save model
if False:
    model = Model()
    obj_save('error_detection_mdl.pkl', model)



# Load model and predict

if False:

    # Start timer
    import time
    start = time.time()

    json_request = obj_load('sine_validation_3.json', path='./ShapeJSON/__downloaded/')
    model_loaded = obj_load('error_detection_mdl.pkl')

    for i in range(1, 2):
        json_response = model_loaded.predict(json_request)
        print(json_response)

    # End timer
    end = time.time()
    print((end - start))


if False:

    # Start timer
    import time
    start = time.time()

    json_request = obj_load('sine_validation_3.json', path='./ShapeJSON/__downloaded/')
    model_loaded = obj_load('mdl00.pkl')

    for i in range(1, 2):
        json_response = model_loaded.predict(json_request)
        print(json_response)

    # End timer
    end = time.time()
    print((end - start))