import requests
import numpy
import json
import matplotlib.pyplot as plt
from build_model import FigureBuilder
from build_shape import Shape
from persistence_manager import obj_load, obj_save

from sklearn import preprocessing


def get_shape_id_list(patient_id):
    url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/patients/%s/tests" % patient_id
    r = requests.get(url)
    json = r.json()

    id_list = []

    for elem in json:
        id_list.append(elem['id'])

    return id_list


def get_single_shape(id, local_id='', local_name=''):
    # url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/582c31bdb0a93f00057b9ba7"
    url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/" + id

    r = requests.get(url)
    data = r.json()
    drawing = data['data']
    points = []

    # obj_save('./ShapeJSON/__downloaded/' + local_name + '_' + str(local_id) + '.json', data)

    for stroke in drawing:
        for point in stroke:
            x = point['x']
            y = point['y']
            t = point['time']
            p = point['force']

            current_point = [x, y, t, p]
            points.append(current_point)

    file_name = local_name + '_' + str(local_id)
    shape = Shape(numpy.asarray(points), file_name)

    return shape


def get_detected_errors(id):
    # url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/%s/error_detection"
    url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/%s/error_detection" % id

    r = requests.get(url)
    data = r.json()
    drawing = data['data']
    points = []

    # obj_save('./ShapeJSON/__downloaded/' + local_name + '_' + str(local_id) + '.json', data)

    for point in drawing:
        x = point['center']['x']
        y = point['center']['y']

        current_point = [x, y]
        points.append(current_point)

    result = numpy.asarray(points)

    return result

plotter = FigureBuilder()

shape_list = get_shape_id_list('validation')

for id in shape_list:
    shape = get_single_shape(id)

    s = shape.data
    x = s[:, 0]
    y = s[:, 1]
    p = s[:, 3]
    # p_normalized = preprocessing.normalize(p.reshape(-1, 1), norm='l1')
    c = get_detected_errors(id)

    plotter.create_figure(id, id)

    plt.subplot(211)
    plt.xlim(20, 1020)
    # plt.ylim(200, 600)

    plt.scatter(x, y, c='k')
    plt.scatter(c[:, 0], c[:, 1], s=1500, facecolors='none', edgecolors='r', linewidth='1.5')

    plt.subplot(212)
    plt.xlim(20, 1020)
    # plt.ylim(200, 600)

    plt.scatter(x, y, c=p, s=(50 * p + 10), edgecolors='none')
    plt.scatter(c[:, 0], c[:, 1], s=1500, facecolors='none', edgecolors='r', linewidth='1.5')


    plotter.save(id)

plotter.show()
# print(shape_list)