__Error Detection AI module__
======

The error detection API’s main responsibility is to implement algorithm, which analyzes and finds location of drawing mistakes, performed during patient drawing tests. Current machine learning algorithm consists of two main parts, all located in module `build_model.py`, however there are other helper modules and functions.
Main two part of the algorithm are following:

* _Model training for drawing errors detection_
* _Data pre-processing and error detection procedure_

Each part is divided into several sub-tasks, some of them are performed manually, some in runtime. Figures below *(Figure 1 and Figure 2)* visualize current algorithm, which consists of several machine learning techniques combined together. Current approach is universal, and can be applied to almost any drawing test, the only restriction is that test should be of tracing-style type (sinus trace, PL curve trace, Poppelreuter test).

### Model training for drawing errors detection:

![Figure 1](./Figures/Doc/fig1.png)

* Pre-processing etalons is done in module `convert_csv_to_json.py` using functions `convert_mm_to_pix_ios(), convert_pix_ios_to_mm()`
	* Etalon drawings are on the background as reference and located in folders `.\ShapeEtalons`
	* Apply scale and offset conversions from iOS measure units to millimeters
	* If needed, interpolate additional points to achieve same density


* Pre-processing training data using functions `convert_mm_to_pix_ios(), convert_pix_ios_to_mm()` in module `convert_csv_to_json.py`
	* Training consists of patient and nonpatient (control) drawings and located in folders `.\ShapeObj` and
	* Each drawing consists of points
	* Apply scale and offset conversions from iOS measure units to millimeters


* Clustering set of points from original shape into N segments with K-means algorithm. This procedure done in module `build_model.py` inside class `Model()` with method `train_kmeans()`
	* Segment is set of continuous points
	* Number of segments is defined experimentally in method `Model.train_kmeans()`
	* Label each segment with class (number from 0 to N)


* Training classification model with *K*-NN algorithm in `Model.train_knn()`
	* For training use reference labels from previous step
	* Number of labels is N
	* Label corresponds to segment class and stored in


* Applying *K*-NN model to classify points for each shape from training data. This is perfeormed in module `build_model.py` inside class `Model()` with method `classify_shapes()`
	* Classify set of points for each shape from training data
	* Classes in the set of points in each shape from training data correspond to reference shape segments
	* Result is clustered drawing from training data


* Compute descriptive statistics parameters to each segment of each drawing using in-class method `classify_shapes()`
* Adding centroid coordinates for each segment of each drawing. Is done in `Model.classify_shapes()`
* Train random forest RF model in `Model.train_rf()`
	* RF model should classify each segment as correct or faulty based on patient and non-patient data

### Data pre-processing and error detection procedure:

![Figure 2](./Figures/Doc/fig2.png)

* Import recorded drawing. Is done through method `predict()` of initialized object of class `Model()`


* Pre-processing of recorded drawing is done in same method `predict()` using helper methods of `Model()` object:
	* Apply scale and offset conversions from iOS measure units to millimeters using module `convert_csv_to_json.py` and functions `convert_mm_to_pix_ios(), convert_pix_ios_to_mm()`


* Classify recorded drawing points by applying *K*-NN model from previous part
	* Get segmented drawing on output from method


* Computing descriptive statistics parameters for each segment of recorded drawing is done in `Model.build_cluster_array()` and `KineticFeatureBuilder.get_features_from_shape()`


* Classify segments by applying random forest RF model from previous part using `Model()` object trained *random forest* model `self.mdl_rf.predict()`
	* Get set of faulty segments along with their centroids with same trained model `self.mdl_rf.predict()`

  
* Return set of faulty segments along with their coordinates


## Requirements:
* **OSX/Linux/Windows** - project can be interpreted in standard Python environment.

#### Install pip:
```bash
    $ sudo easy_install pip
    $ sudo pip install --upgrade virtualenv
```

#### Install numpy:
```bash
    $ pip install numpy
```

#### Install pandas:
```bash
    # Ubuntu/Linux 64-bit/Mac OS X
    $ pip install pandas
```

#### Install matplotlib:
```bash
    # Python
    $ pip3 install matplotlib
```

#### Install scikit-learn
```bash
    # Python
    $ pip3 install -U scikit-learn
```

#### Or you can install all project dependencies:
```bash
    # Python
    $ pip3 install -r requirements.txt
```
