import requests
import numpy
import matplotlib.pyplot as plt
import pickle
import os

# from build_model import FigureBuilder
from build_shape import Shape
from numpy import asarray

def obj_save(filename, obj):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def obj_load(filename, path=''):
    with open(path + filename, 'rb') as input:
        # print(input)
        obj = pickle.load(input)
        print(type(obj))
        return obj


def obj_load_multiple(path, ext):
    arr_list = []
    for file in os.listdir(path):
        if file.endswith(ext):
            arr = obj_load(file, path)
            arr_list.append(arr)
    return arr_list

if False:
    sample_arr1 = asarray([1, 2, 3, 4])
    shape1 = Shape(sample_arr1, 'shape1')

    obj_save('shape2.pkl')
    shape3 = obj_load('shape2.pkl')
    print(shape3.filename)