import numpy as np

# x = np.array([np.nan, -np.inf, 0., np.inf, 1.0, 1.2, np.nan])
# y = np.array([2, 2, 2])
# res = np.nansum(x[np.isfinite(x)])

# print(res)
a = np.asarray([1, 2, 3, 4, 5])
b = np.asarray([0, 0, 0, 1, 1])

mask = np.where(b == 1)

# print(mask)
print(a[mask])


a = np.asarray([[11, 21, 31], [12, 22, 32], [13, 23, 33]])
b = np.asarray([41, 42, 43])

# print(a)
# print(a[:, 1])

b = np.array([b]).T
# print(b)

c = np.concatenate((a, b), axis=1)
# print(c)
#
# def join_arr_from_right_side(arr1, arr2):
#     return np.concatenate((arr1, arr2.T), axis=1)

# a = np.array([np.ones(25)]).T
# print(a)

# print(c)

# a = np.asarray([[11, 21, 31], [12, 22, 32], [13, 23, 33]])
a = np.empty((0, 3), dtype=float)
b = np.asarray([41, 42, 43])


# print(np.vstack((a, b)))