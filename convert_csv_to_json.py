from pandas import DataFrame, read_csv

import os
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
matplotlib.style.use('ggplot')
import numpy as np
import json

dir_collected = "./ShapeCollectedData/"
dir_etalons = "./ShapeEtalons/"
dir_json = "./ShapeJSON/"

plt.figure(1)
plt.axis([-100,1024+100, 0-100, 768+100])

# Convert from Android pix to mm

def print_shape_params(shape):
    print("Shape:")
    print("---------------------")
    print("MaxX:    " + str(max(shape.X)))
    print("MinX:    " + str(min(shape.X)))
    print("---------------------")
    print("MaxY:    " + str(max(shape.Y)))
    print("MinY:    " + str(min(shape.Y)) + "\n")

def convert_pix_android_to_mm(data_frame):
    data_frame.X /= (149.82489 / 25.4)
    data_frame.Y /= (149.82489 / 25.4)

    # Reduce size to fit iPad Screen
    data_frame.X *= 0.95
    data_frame.Y *= 0.95
    return data_frame

def convert_mm_to_pix_ios(data_frame):
    data_frame.X /= (25.4 / 264) * 2
    data_frame.Y /= (25.4 / 264) * 2
    return data_frame

def convert_pix_ios_to_mm(data_frame):
    data_frame.X *= (25.4 / 264) * 2
    data_frame.Y *= (25.4 / 264) * 2
    return data_frame

def calculate_offset(data_frame, width, height):
    min_x = min(data_frame.X)
    min_y = min(data_frame.Y)
    max_x = max(data_frame.X)
    max_y = max(data_frame.Y)
    offset_x = (max_x + min_x) / 2.0 - width / 2.0
    offset_y = (max_y + min_y) / 2.0 - height / 2.0
    return offset_x, offset_y

def center_shape(data_frame, width, height):
    offset_x, offset_y = calculate_offset(data_frame, width, height)
    data_frame.X -= offset_x
    data_frame.Y -= offset_y
    return data_frame

def convert_to_json(data_frame, name="none", folder="./"):
    data = []
    for index in range(0, len(data_frame.X)):
        x = data_frame.X[index]
        y = data_frame.Y[index]
        point = {"x": x,
                 "y": y,
                 "force": 0,
                 "aziang": 0,
                 "altang": 0,
                 "time": 0}
        data.append(point)
        # print(str(data_frame.X[index]) + " " + str(data_frame.Y[index]))

    txtfile = open(folder + name + ".json", "w")
    # print(txtfile)
    shape_json = json.dump({"data": [data],
                            "id": name,
                            "time": "2016-10-10 10:10:10 +0000",
                            "patientId": name,
                            "type": name}, txtfile, sort_keys=True, indent=4)
    # print(shape_json)
    return shape_json

for file in os.listdir(dir_etalons):
    if file.endswith(".csv"):

        shape_name = file[0:-4]
        # print(dir_etalons + file)

        df = pd.read_csv(dir_etalons + file, names=['X', 'Y'])
        df = convert_pix_android_to_mm(df)
        df = convert_mm_to_pix_ios(df)

        # print_shape_params(df)
        offset_x, offset_y = calculate_offset(df, 1024, 768)
        df.Y -= offset_y

        if ("copy" in shape_name):
            print("Offset y with: " + shape_name)
            df.Y += 250

        if not ("continue" in shape_name):
            print("Offset x with: " + shape_name)
            df.X -= offset_x

        df.Y = df.Y * -1.0 + 786
        plt.plot(df.X, df.Y)

        convert_to_json(df, name=shape_name, folder=dir_json)

plt.show()