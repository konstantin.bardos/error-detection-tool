### Single drawing download
### http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/582c31bdb0a93f00057b9ba7

### Get drawing id-s from patientId
### http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/patients/1234/tests

import requests
import numpy
import json
import matplotlib.pyplot as plt
from build_model import FigureBuilder
from build_shape import Shape
from persistence_manager import obj_load, obj_save


def download_remote(remote_name, local_name, local_path):

    def get_shape_id_list(patient_id):

        url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/patients/%s/tests" % patient_id
        r = requests.get(url)
        json = r.json()

        id_list = []

        for elem in json:
            id_list.append(elem['id'])

        return id_list

    def get_single_shape(id, local_id, local_name):

        # url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/582c31bdb0a93f00057b9ba7"
        url = "http://ec2-54-93-99-128.eu-central-1.compute.amazonaws.com/tests/" + id

        r = requests.get(url)
        data = r.json()
        drawing = data['data']
        points = []

        obj_save('./ShapeJSON/__downloaded/' + local_name + '_' + str(local_id) + '.json', data)

        for stroke in drawing:
            for point in stroke:

                x = point['x']
                y = point['y']
                t = point['time']
                p = point['force']

                current_point = [x, y, t, p]
                points.append(current_point)

        file_name = local_name + '_' + str(local_id)
        shape = Shape(numpy.asarray(points), file_name)

        return shape

    id_list = get_shape_id_list(remote_name)

    # print(id_list)

    shapes = []
    index = 0

    for id in id_list:

        index += 1

        shape = get_single_shape(id, index, local_name)
        obj_save(local_path + shape.filename + '.pkl', shape)
        shapes.append(shape)

    if True:
        plotter = FigureBuilder()
        index = 0

        for shape in shapes:
            index += 1
            plotter.create_figure(index, shape.filename)

            data = shape.data
            x = data[:, 0]
            y = data[:, 1]

            plt.scatter(x, y)
            plotter.save(shape.filename)

        # plotter.show()


def save_original_shape():

    with open('./ShapeObj/sine_trace.json') as data_file:
        data = json.load(data_file)

    drawing = data['data']
    points = []

    for stroke in drawing:
        for point in stroke:

            x = point['x']
            y = point['y']

            current_point = [x, y]
            points.append(current_point)

    shape = Shape(numpy.asarray(points), 'sine_trace')
    obj_save('./ShapeObj/' + shape.filename + '.pkl', shape)

    if True:
        plotter = FigureBuilder()
        plotter.create_figure(100, shape.filename)

        data = shape.data
        x = data[:, 0]
        y = data[:, 1]

        plt.scatter(x, y)
        plotter.save(shape.filename)
        plotter.show()

# download_remote('control', 'sine_correct', './ShapeObj/sine_correct/')
# download_remote('error', 'sine_error', './ShapeObj/sine_error/')
download_remote('validation', 'sine_validation', './ShapeObj/sine_validation/')
# save_original_shape()